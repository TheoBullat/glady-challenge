package org.perso.theo.bullat.controller;

import io.quarkus.test.InjectMock;
import io.quarkus.test.common.http.TestHTTPEndpoint;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.perso.theo.bullat.dto.BalanceDTO;
import org.perso.theo.bullat.dto.BalanceDetailsDTO;
import org.perso.theo.bullat.service.UserDepositsService;
import org.mockito.Mockito;

import java.time.LocalDate;
import java.util.Collections;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;
import static org.mockito.ArgumentMatchers.*;
import static org.perso.theo.bullat.service.TokenGeneratorService.ROLE_USER;

@QuarkusTest
@TestHTTPEndpoint(GiftDepositsController.class)
class GETGiftDepositsControllerTest {

    private static final String TEST_USERNAME = "testUser";

    @InjectMock
    UserDepositsService depositsService;

    @BeforeEach
    public void setup() {
        Mockito.when(depositsService.getBalance(anyString(), any())).thenReturn(new BalanceDTO()
                .setUsername(TEST_USERNAME)
                .setDetails(Collections.singletonList(
                        new BalanceDetailsDTO()
                                .setExpirationDate(LocalDate.of(2024,1,1))
                                .setAmount(50L))));
    }

    @Test
    @TestSecurity(user = TEST_USERNAME, roles = {ROLE_USER})
    void testGetGift_WithRole_OK() {
        given()
                .when().get()
                .then()
                .statusCode(200)
                .body(is("""
                        {"username":"testUser","details":[{"expirationDate":"2024-01-01","amount":50}]}"""));
    }

    @Test
    void testGetGift_Unauthorized_KO() {
        given()
                .when().get()
                .then()
                .statusCode(401)
                .body(is("""
                        {"httpMethod":"GET","resource":"/gift","status":"Unauthorized"}"""));
    }

    @Test
    @TestSecurity(user = TEST_USERNAME)
    void testGetGift_Forbidden_KO() {
        given()
                .when().get()
                .then()
                .statusCode(403)
                .body(is("""
                        {"httpMethod":"GET","resource":"/gift","status":"Forbidden"}"""));
    }

}