package org.perso.theo.bullat.controller;


import io.quarkus.test.InjectMock;
import io.quarkus.test.common.http.TestHTTPEndpoint;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;
import io.quarkus.test.security.jwt.Claim;
import io.quarkus.test.security.jwt.JwtSecurity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.perso.theo.bullat.service.CompanyBalanceService;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.perso.theo.bullat.service.TokenGeneratorService.*;

@QuarkusTest
@TestHTTPEndpoint(CompanyBalanceController.class)
class POSTCompanyBalanceControllerTest {
    private static final String TEST_USERNAME = "testUser";
    private static final String TEST_USERNAME_MANAGER = "testUserManager";

    private static final String TEST_COMPANY = "Tesla";
    
    @InjectMock
    CompanyBalanceService companyBalanceService;

    @BeforeEach
    public void setup() {
        Mockito.when(companyBalanceService.creditCompany(anyString(), any()))
                .thenReturn(100L);
    }
    
    @Test
    @TestSecurity(user = TEST_USERNAME_MANAGER, roles = {ROLE_COMPANY})
    @JwtSecurity(claims = {
            @Claim(key = COMPANY_MANAGER, value = TEST_COMPANY)
    })
    void testPostCompanyBalance_WithRole_OK() {
        given()
                .when()
                .post("/credit/100")
                .then()
                .statusCode(200)
                .body(is("""
                        New balance for Tesla is 100"""));
    }

    @Test
    void testPostCompanyBalance_Unauthorized_KO() {
        given()
                .when()
                .post("/credit/100")
                .then()
                .statusCode(401)
                .body(is("""
                        {"httpMethod":"POST","resource":"/company/credit/100","status":"Unauthorized"}"""));
    }

    @Test
    @TestSecurity(user = TEST_USERNAME, roles = {ROLE_USER})
    void testPostCompanyBalance_Forbidden_KO() {
        given()
                .when()
                .post("/credit/100")
                .then()
                .statusCode(403)
                .body(is("""
                        {"httpMethod":"POST","resource":"/company/credit/100","status":"Forbidden"}"""));
    }
}
