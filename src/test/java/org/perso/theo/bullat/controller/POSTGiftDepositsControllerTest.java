package org.perso.theo.bullat.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.quarkus.test.InjectMock;
import io.quarkus.test.common.http.TestHTTPEndpoint;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.security.TestSecurity;
import io.quarkus.test.security.jwt.Claim;
import io.quarkus.test.security.jwt.JwtSecurity;
import io.restassured.http.ContentType;
import jakarta.inject.Inject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.perso.theo.bullat.dto.BalanceDTO;
import org.perso.theo.bullat.dto.BalanceDetailsDTO;
import org.perso.theo.bullat.dto.DepositDTO;
import org.perso.theo.bullat.exception.NotEnoughBalanceAvailable;
import org.perso.theo.bullat.service.UserDepositsService;

import java.time.LocalDate;
import java.util.Collections;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.perso.theo.bullat.service.TokenGeneratorService.*;

@QuarkusTest
@TestHTTPEndpoint(GiftDepositsController.class)
class POSTGiftDepositsControllerTest {

    private static final String TEST_USERNAME = "testUser";
    private static final String TEST_USERNAME_MANAGER = "testUserManager";
    private static final String TEST_USERNAME_CREDITED = "testUserCredited";

    private static final String TEST_COMPANY = "Tesla";

    @InjectMock
    UserDepositsService depositsService;

    @Inject
    ObjectMapper objectMapper;

    private String body;

    @BeforeEach
    public void setup() throws JsonProcessingException {
        Mockito.when(depositsService.creditUser(any(), any(), anyString(), anyString()))
                // First call has enough balance
                .thenReturn(new BalanceDTO()
                        .setUsername(TEST_USERNAME_CREDITED)
                        .setDetails(Collections.singletonList(
                                new BalanceDetailsDTO()
                                        .setExpirationDate(LocalDate.of(2024, 1, 1))
                                        .setAmount(50L))))
                // Second has not enough balance anymore and should fail
                .thenThrow(new NotEnoughBalanceAvailable(String.format("%s has not enough balance to do the transaction", TEST_COMPANY)));
        DepositDTO deposit = new DepositDTO()
                .setAmount(50L)
                .setUsername(TEST_USERNAME_CREDITED);
        body = objectMapper.writeValueAsString(deposit);
    }

    @Test
    @TestSecurity(user = TEST_USERNAME_MANAGER, roles = {ROLE_COMPANY})
    @JwtSecurity(claims = {
            @Claim(key = COMPANY_MANAGER, value = TEST_COMPANY)
    })
    void testPostGift_WithRole_OK() {
        // First call has enough balance
        given()
                .when()
                .body(body)
                .contentType(ContentType.JSON)
                .post()
                .then()
                .statusCode(200)
                .body(is("""
                        {"username":"testUserCredited","details":[{"expirationDate":"2024-01-01","amount":50}]}"""));

        // Second has not enough balance anymore and should fail
        given()
                .when()
                .body(body)
                .contentType(ContentType.JSON)
                .post()
                .then()
                .statusCode(400)
                .body(is("""
                        {"httpMethod":"POST","resource":"/gift","status":"Bad Request","message":"Tesla has not enough balance to do the transaction"}"""));
    }

    @Test
    void testPostGift_Unauthorized_KO() {
        given()
                .when()
                .body(body)
                .contentType(ContentType.JSON)
                .post()
                .then()
                .statusCode(401)
                .body(is("""
                        {"httpMethod":"POST","resource":"/gift","status":"Unauthorized"}"""));
    }

    @Test
    @TestSecurity(user = TEST_USERNAME, roles = {ROLE_USER})
    void testPostGift_Forbidden_KO() {
        given()
                .when()
                .body(body)
                .contentType(ContentType.JSON)
                .post()
                .then()
                .statusCode(403)
                .body(is("""
                        {"httpMethod":"POST","resource":"/gift","status":"Forbidden"}"""));
    }

}