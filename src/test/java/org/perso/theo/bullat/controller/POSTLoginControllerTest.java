package org.perso.theo.bullat.controller;


import io.quarkus.security.ForbiddenException;
import io.quarkus.test.InjectMock;
import io.quarkus.test.common.http.TestHTTPEndpoint;
import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.perso.theo.bullat.service.TokenGeneratorService;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;


@QuarkusTest
@TestHTTPEndpoint(LoginController.class)
class POSTLoginControllerTest {

    @InjectMock
    TokenGeneratorService tokenGeneratorService;

    @BeforeEach
    public void setup() {
        Mockito.doNothing().when(tokenGeneratorService).checkUserCredentials(eq("secured"));
        Mockito.doThrow(new ForbiddenException("Password is not 'secured'")).when(tokenGeneratorService).checkUserCredentials(eq("unsafe"));
        Mockito.doReturn("eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.fakeToken").when(tokenGeneratorService).generateTokenWithRole(any());
    }

    @Test
    void testLogin_withSecuredPwd_OK() {
        String goodAuthent = """
                {"username": "testUser", "password": "secured"}""";
        given()
                .when()
                .body(goodAuthent)
                .contentType(ContentType.JSON)
                .post()
                .then()
                .statusCode(200)
                .body(is("eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.fakeToken"));
    }

    @Test
    void testLogin_withUnsafePwd_OK() {
        String badAuthent = """
                {"username": "testUser", "password": "unsafe"}""";
        given()
                .when()
                .body(badAuthent)
                .contentType(ContentType.JSON)
                .post()
                .then()
                .statusCode(403)
                .body(is("""
                        {"httpMethod":"POST","resource":"/login","status":"Forbidden","message":"Password is not 'secured'"}"""));
    }
}
