package org.perso.theo.bullat.service;

import io.quarkus.panache.mock.PanacheMock;
import io.quarkus.test.InjectMock;
import io.quarkus.test.junit.QuarkusTest;
import jakarta.inject.Inject;
import org.hibernate.Session;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.perso.theo.bullat.dao.CompanyBalanceDAO;
import org.perso.theo.bullat.exception.NotEnoughBalanceAvailable;

import java.util.Optional;

import static org.mockito.ArgumentMatchers.anyString;

@QuarkusTest
class CompanyBalanceServiceTest {

    private final String TEST_COMPANY = "testCompany";

    @Inject
    CompanyBalanceService companyBalanceService;

    @InjectMock
    Session session;

    @Test
    void testCreditCompany() {
        PanacheMock.mock(CompanyBalanceDAO.class);
        CompanyBalanceDAO companyBalance = new CompanyBalanceDAO()
                .setCompany(TEST_COMPANY)
                .setAmount(50L);
        Mockito.when(CompanyBalanceDAO.findByCompany(anyString()))
                // At first company doesn't exist
                .thenReturn(Optional.empty())
                // Then we can find it;
                .thenReturn(Optional.of(companyBalance));

        Mockito.doNothing().when(session).persist(Mockito.any());

        Long finalAmount = companyBalanceService.creditCompany(TEST_COMPANY, 50L);
        Assertions.assertEquals(50L, finalAmount);

        finalAmount = companyBalanceService.creditCompany(TEST_COMPANY, 100L);
        Assertions.assertEquals(150L, finalAmount);
    }


    @Test
    void testRemoveBalanceFromCompany() {
        PanacheMock.mock(CompanyBalanceDAO.class);
        CompanyBalanceDAO companyBalanceEmpty = new CompanyBalanceDAO()
                .setCompany(TEST_COMPANY)
                .setAmount(0L);
        CompanyBalanceDAO companyBalance = new CompanyBalanceDAO()
                .setCompany(TEST_COMPANY)
                .setAmount(50L);

        Mockito.when(CompanyBalanceDAO.findByCompany(anyString()))
                // At first company doesn't exist
                .thenReturn(Optional.empty())
                // Then we can find it but with not enough balance
                .thenReturn(Optional.of(companyBalanceEmpty))
                // And finally we find it with enough balance
                .thenReturn(Optional.of(companyBalance));

        Mockito.doNothing().when(session).persist(Mockito.any());

        Assertions.assertThrows(NotEnoughBalanceAvailable.class, () -> companyBalanceService.removeBalanceFromCompany(TEST_COMPANY, 50L));
        Assertions.assertThrows(NotEnoughBalanceAvailable.class, () -> companyBalanceService.removeBalanceFromCompany(TEST_COMPANY, 50L));
        Assertions.assertDoesNotThrow(() -> companyBalanceService.removeBalanceFromCompany(TEST_COMPANY, 50L));
    }

}
