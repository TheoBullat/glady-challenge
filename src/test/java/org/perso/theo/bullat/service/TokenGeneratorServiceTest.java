package org.perso.theo.bullat.service;

import io.quarkus.security.ForbiddenException;
import io.quarkus.test.junit.QuarkusTest;
import io.vertx.core.json.JsonObject;
import jakarta.inject.Inject;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.perso.theo.bullat.dto.AuthenticationDTO;

import java.util.Base64;
import java.util.List;

import static org.perso.theo.bullat.service.TokenGeneratorService.*;

@QuarkusTest
class TokenGeneratorServiceTest {

    private static final String TEST_USERNAME = "testUsers";
    private static final String TEST_COMPANY = "testCompany";
    private static final String TEST_PASSWORD = "secured";

    @Inject
    TokenGeneratorService tokenGeneratorService;


    @Test
    void testGenerateTokenWithoutCompany() {
        AuthenticationDTO authent = new AuthenticationDTO()
                .setUsername(TEST_USERNAME)
                .setPassword(TEST_PASSWORD)
                .setCompany(null);

        String jwtToken = tokenGeneratorService.generateTokenWithRole(authent);
        String[] chunks = jwtToken.split("\\.");
        Base64.Decoder decoder = Base64.getUrlDecoder();
        JsonObject payload = new JsonObject(new String(decoder.decode(chunks[1])));

        List<String> groups = payload.getJsonArray("groups").getList();
        Assertions.assertEquals( 1, groups.size());
        Assertions.assertEquals(ROLE_USER, groups.get(0));

        Assertions.assertEquals(TEST_USERNAME, payload.getString("upn"));
    }



    @Test
    void testGenerateTokenWithCompany() {
        AuthenticationDTO authent = new AuthenticationDTO()
                .setUsername(TEST_USERNAME)
                .setPassword(TEST_PASSWORD)
                .setCompany(TEST_COMPANY);

        String jwtToken = tokenGeneratorService.generateTokenWithRole(authent);
        String[] chunks = jwtToken.split("\\.");
        Base64.Decoder decoder = Base64.getUrlDecoder();
        JsonObject payload = new JsonObject(new String(decoder.decode(chunks[1])));

        List<String> groups = payload.getJsonArray("groups").getList();
        Assertions.assertEquals(2, groups.size());
        Assertions.assertEquals(ROLE_COMPANY, groups.get(0));
        Assertions.assertEquals(ROLE_USER, groups.get(1));

        Assertions.assertEquals(TEST_USERNAME, payload.getString("upn"));

        Assertions.assertEquals(TEST_COMPANY, payload.getString(COMPANY_MANAGER));
    }

    @Test
    void testCheckUserCredentials() {
        Assertions.assertThrows(ForbiddenException.class, () -> tokenGeneratorService.checkUserCredentials("unsafe"));
        Assertions.assertDoesNotThrow(() -> tokenGeneratorService.checkUserCredentials("secured"));
    }
}
