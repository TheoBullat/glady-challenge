package org.perso.theo.bullat.service;

import io.quarkus.panache.mock.PanacheMock;
import io.quarkus.test.InjectMock;
import io.quarkus.test.junit.QuarkusTest;
import jakarta.inject.Inject;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.perso.theo.bullat.dao.CompanyBalanceDAO;
import org.perso.theo.bullat.dao.UserDepositsDAO;
import org.perso.theo.bullat.dao.domain.TypeEnum;
import org.perso.theo.bullat.dto.BalanceDTO;
import org.perso.theo.bullat.dto.DepositDTO;
import org.perso.theo.bullat.exception.NotEnoughBalanceAvailable;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;

@QuarkusTest
class UserDepositsServiceTest {

    private static final String TEST_USER = "TestUser";
    private static final String TEST_ACTUAL_USER = "TestActualUser";
    private static final String TEST_COMPANY = "TestCompany";

    @Inject
    UserDepositsService userDepositsService;

    @InjectMock
    CompanyBalanceService companyBalanceService;

    @Test
    void testCreditUser_companyHasBalance_GIFT() {
        test_companyHasBalance(TypeEnum.GIFT);
    }

    @Test
    void testCreditUser_companyDoesntHaveBalance_GIFT() {
        test_companyDoesntHaveBalance(TypeEnum.GIFT);
    }

    @Test
    void testCreditUser_companyHasBalance_MEAL() {
        test_companyHasBalance(TypeEnum.MEAL);
    }

    @Test
    void testCreditUser_companyDoesntHaveBalance_MEAL() {
        test_companyDoesntHaveBalance(TypeEnum.MEAL);
    }

    private void test_companyHasBalance(TypeEnum type) {
        Mockito.doNothing().when(companyBalanceService).removeBalanceFromCompany(anyString(), any());

        DepositDTO deposit = new DepositDTO()
                .setUsername(TEST_USER)
                .setAmount(50L);
        BalanceDTO balanceDTO = userDepositsService.creditUser(deposit, type, TEST_ACTUAL_USER, TEST_COMPANY);

        Assertions.assertEquals(TEST_USER, balanceDTO.getUsername());
        Assertions.assertEquals(1, balanceDTO.getDetails().size());
        Assertions.assertEquals(50L, balanceDTO.getDetails().get(0).getAmount());
        Assertions.assertNotNull(balanceDTO.getDetails().get(0).getExpirationDate());
    }

    private void test_companyDoesntHaveBalance(TypeEnum type) {
        Mockito.doThrow(new NotEnoughBalanceAvailable(String.format("%s has not enough balance to do the transaction", TEST_COMPANY))).when(companyBalanceService).removeBalanceFromCompany(anyString(), any());

        DepositDTO deposit = new DepositDTO()
                .setUsername(TEST_USER)
                .setAmount(50L);
        Assertions.assertThrows(NotEnoughBalanceAvailable.class, () -> userDepositsService.creditUser(deposit, type, TEST_ACTUAL_USER, TEST_COMPANY));
    }

    @Test
    void testGetBalance() {
        int nextYear = LocalDate.now().plusYears(1).getYear();

        UserDepositsDAO userDeposits1 = new UserDepositsDAO()
                .setUsername(TEST_USER)
                .setAmount(50L)
                .setExpireAt(LocalDate.of(nextYear, 1, 1))
                .setType(TypeEnum.GIFT)
                .setCreditedBy(TEST_ACTUAL_USER)
                .setCreditedAt(LocalDateTime.now());

        UserDepositsDAO userDeposits2 = new UserDepositsDAO()
                .setUsername(TEST_USER)
                .setAmount(50L)
                .setExpireAt(LocalDate.of(nextYear, 1, 1))
                .setType(TypeEnum.GIFT)
                .setCreditedBy(TEST_ACTUAL_USER)
                .setCreditedAt(LocalDateTime.now());

        UserDepositsDAO userDeposits3 = new UserDepositsDAO()
                .setUsername(TEST_USER)
                .setAmount(50L)
                .setExpireAt(LocalDate.of(nextYear, 6, 6))
                .setType(TypeEnum.GIFT)
                .setCreditedBy(TEST_ACTUAL_USER)
                .setCreditedAt(LocalDateTime.now());

        PanacheMock.mock(UserDepositsDAO.class);
        Mockito.when(UserDepositsDAO.findNotExpiredDepositsByType(anyString(), any()))
                .thenReturn(Arrays.asList(userDeposits1, userDeposits2, userDeposits3));

        BalanceDTO balance = userDepositsService.getBalance(TEST_USER, TypeEnum.GIFT);

        Assertions.assertEquals(TEST_USER, balance.getUsername());
        Assertions.assertEquals(2, balance.getDetails().size());
        Assertions.assertEquals(50L, balance.getDetails().get(0).getAmount());
        Assertions.assertEquals(100L, balance.getDetails().get(1).getAmount());
        Assertions.assertEquals(LocalDate.of(nextYear, 6, 6), balance.getDetails().get(0).getExpirationDate());
        Assertions.assertEquals(LocalDate.of(nextYear, 1, 1), balance.getDetails().get(1).getExpirationDate());
    }
}
