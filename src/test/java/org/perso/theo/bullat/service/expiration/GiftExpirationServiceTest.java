package org.perso.theo.bullat.service.expiration;

import io.quarkus.test.junit.QuarkusTest;
import jakarta.inject.Inject;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

@QuarkusTest
class GiftExpirationServiceTest {

    @Inject
    GiftExpirationService giftExpirationService;

    @Test
    void testGenerateExpirationDateForGift() {
        LocalDate from = LocalDate.of(2021, 6, 15);

        LocalDate expirationDate = giftExpirationService.generateExpirationDate(from);
        Assertions.assertEquals(2022, expirationDate.getYear());
        Assertions.assertEquals(6, expirationDate.getMonthValue());
        Assertions.assertEquals(14, expirationDate.getDayOfMonth());
    }

    @Test
    void testGenerateExpirationDateForGift_leapYear() {
        LocalDate from = LocalDate.of(2023, 6, 15);

        LocalDate expirationDate = giftExpirationService.generateExpirationDate(from);
        Assertions.assertEquals(2024, expirationDate.getYear());
        Assertions.assertEquals(6, expirationDate.getMonthValue());
        Assertions.assertEquals(13, expirationDate.getDayOfMonth());
    }
}
