package org.perso.theo.bullat.service.expiration;

import io.quarkus.test.junit.QuarkusTest;
import jakarta.inject.Inject;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

@QuarkusTest
class MealExpirationServiceTest {

    @Inject
    MealExpirationService mealExpirationService;

    @Test
    void testGenerateExpirationDateForMeal() {
        LocalDate from = LocalDate.of(2020, 1, 1);

        LocalDate expirationDate = mealExpirationService.generateExpirationDate(from);
        Assertions.assertEquals(2021, expirationDate.getYear());
        Assertions.assertEquals(2, expirationDate.getMonthValue());
        Assertions.assertEquals(28, expirationDate.getDayOfMonth());
    }

    @Test
    void testGenerateExpirationDateForMeal_leapYear() {
        LocalDate from = LocalDate.of(2019, 1, 1);

        LocalDate expirationDate = mealExpirationService.generateExpirationDate(from);
        Assertions.assertEquals(2020, expirationDate.getYear());
        Assertions.assertEquals(2, expirationDate.getMonthValue());
        Assertions.assertEquals(29, expirationDate.getDayOfMonth());
    }

    @Test
    void testGenerateExpirationDateForMeal_endOfTheYear() {
        LocalDate from = LocalDate.of(2020, 12, 31);

        LocalDate expirationDate = mealExpirationService.generateExpirationDate(from);
        Assertions.assertEquals(2021, expirationDate.getYear());
        Assertions.assertEquals(2, expirationDate.getMonthValue());
        Assertions.assertEquals(28, expirationDate.getDayOfMonth());
    }
}
