package org.perso.theo.bullat.service;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.inject.spi.CDI;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import org.perso.theo.bullat.dao.UserDepositsDAO;
import org.perso.theo.bullat.dao.domain.TypeEnum;
import org.perso.theo.bullat.dto.BalanceDTO;
import org.perso.theo.bullat.dto.BalanceDetailsDTO;
import org.perso.theo.bullat.dto.DepositDTO;
import org.perso.theo.bullat.service.expiration.ExpirationService;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service that allow a user to make a deposit for an other user. You may look for your own balance as well
 */
@ApplicationScoped
public class UserDepositsService {

    @Inject
    CompanyBalanceService companyBalanceService;

    @Transactional
    public BalanceDTO creditUser(DepositDTO deposit, TypeEnum type, String actualUser, String company) {
        companyBalanceService.removeBalanceFromCompany(company, deposit.getAmount());

        ExpirationService expirationService = CDI.current().select(type.getExpirationService()).get();
        LocalDate expirationDate = expirationService.generateExpirationDate(LocalDate.now());
        UserDepositsDAO userDeposits = new UserDepositsDAO()
                .setUsername(deposit.getUsername())
                .setAmount(deposit.getAmount())
                .setType(type)
                .setExpireAt(expirationDate)
                .setCreditedBy(actualUser)
                .setCreditedAt(LocalDateTime.now());

        userDeposits.persist();

        return new BalanceDTO()
                .setUsername(deposit.getUsername())
                .setDetails(Collections.singletonList(
                        new BalanceDetailsDTO()
                                .setAmount(deposit.getAmount())
                                .setExpirationDate(expirationDate)
                ));
    }


    public BalanceDTO getBalance(String username, TypeEnum type) {
        List<UserDepositsDAO> deposits = UserDepositsDAO.findNotExpiredDepositsByType(username, type);
        List<BalanceDetailsDTO> details = deposits.stream()
                // We group and sum amount by expirationdate
                .collect(Collectors.groupingBy(
                        UserDepositsDAO::getExpireAt,
                        Collectors.summingLong(UserDepositsDAO::getAmount)))
                .entrySet().stream()
                // Transform them into DTO
                .map(entry -> new BalanceDetailsDTO()
                        .setExpirationDate(entry.getKey())
                        .setAmount(entry.getValue())
                ).toList();

        return new BalanceDTO()
                .setUsername(username)
                .setDetails(details);
    }
}
