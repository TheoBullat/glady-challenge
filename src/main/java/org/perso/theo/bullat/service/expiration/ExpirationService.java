package org.perso.theo.bullat.service.expiration;

import java.time.LocalDate;

/**
 * Interface that represent a service able to compute an expiration date
 */
public interface ExpirationService {

    LocalDate generateExpirationDate(LocalDate from);
}
