package org.perso.theo.bullat.service.expiration;

import io.quarkus.arc.Unremovable;
import jakarta.enterprise.context.ApplicationScoped;

import java.time.LocalDate;
import java.time.Month;
import java.time.YearMonth;


/**
 * Service able to compute the expiration date for a meal deposit
 */
@ApplicationScoped
@Unremovable
public class MealExpirationService implements ExpirationService {
    @Override
    public LocalDate generateExpirationDate(LocalDate from) {
        // We get the following year
        int followingYear = from
                .plusYears(1)
                .getYear();
        // We search for the end of the month on this following year
        return YearMonth.of(followingYear, Month.FEBRUARY)
                .atEndOfMonth();
    }
}
