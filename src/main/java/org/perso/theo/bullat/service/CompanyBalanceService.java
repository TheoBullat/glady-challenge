package org.perso.theo.bullat.service;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.transaction.Transactional;
import org.perso.theo.bullat.dao.CompanyBalanceDAO;
import org.perso.theo.bullat.exception.NotEnoughBalanceAvailable;

import java.util.Optional;

/**
 * Service that allow a user to add or remove credit to the balance of a company
 */
@ApplicationScoped
public class CompanyBalanceService {


    @Transactional
    public Long creditCompany(String company, Long amount) {
        Optional<CompanyBalanceDAO> companyBalance = CompanyBalanceDAO.findByCompany(company);
        if (companyBalance.isPresent()) {
            CompanyBalanceDAO actualCompanyBalance = companyBalance.get();
            actualCompanyBalance.setAmount(actualCompanyBalance.getAmount() + amount);
            actualCompanyBalance.persist();

            return actualCompanyBalance.getAmount();
        } else {
            CompanyBalanceDAO newCompanyBalance = new CompanyBalanceDAO()
                    .setCompany(company)
                    .setAmount(amount);
            newCompanyBalance.persist();

            return newCompanyBalance.getAmount();
        }
    }


    @Transactional
    public void removeBalanceFromCompany(String company, Long amount) {
        Optional<CompanyBalanceDAO> companyBalanceOpt = CompanyBalanceDAO.findByCompany(company);
        if (companyBalanceOpt.isPresent()) {
            CompanyBalanceDAO companyBalance = companyBalanceOpt.get();
            if (companyBalance.getAmount() >= amount) {
                companyBalance.setAmount(companyBalance.getAmount() - amount);
                companyBalance.persist();
                return;
            }
        }

        throw new NotEnoughBalanceAvailable(String.format("%s has not enough balance to do the transaction", company));
    }
}
