package org.perso.theo.bullat.service;

import io.quarkus.security.ForbiddenException;
import io.smallrye.jwt.build.Jwt;
import io.smallrye.jwt.build.JwtClaimsBuilder;
import jakarta.enterprise.context.ApplicationScoped;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.perso.theo.bullat.dto.AuthenticationDTO;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Service that allow a user to generate a token with the role User and potentially Company
 * For a role Company, the name of the company is added as a claim in the jwtToken
 */
@ApplicationScoped
public class TokenGeneratorService {
    public static final String ROLE_USER = "User";
    public static final String ROLE_COMPANY = "Company";

    public static final String COMPANY_MANAGER = "company";

    @ConfigProperty(name = "jwt.issuer")
    String issuer;

    public String generateTokenWithRole(AuthenticationDTO authent) {
        JwtClaimsBuilder jwtBuilder = Jwt.issuer(issuer)
                .upn(authent.getUsername());

        // We give the user role by default
        Set<String> roles = new HashSet<>(List.of(ROLE_USER));
        if (StringUtils.isNotBlank(authent.getCompany())) {
            // If a company is given, we give the ability to manage this company
            roles.add(ROLE_COMPANY);
            jwtBuilder.claim(COMPANY_MANAGER, authent.getCompany());
        }
        jwtBuilder.groups(roles);

        return jwtBuilder.sign();
    }

    /**
     * This is a totally safe way to check if a password is good and secured
     *
     * @param authent The user authentication
     */
    public void checkUserCredentials(String password) {
        if (!"secured".equals(password)) {
            throw new ForbiddenException("Password is not 'secured'");
        }
    }
}
