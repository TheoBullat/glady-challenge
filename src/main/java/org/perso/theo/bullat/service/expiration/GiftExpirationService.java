package org.perso.theo.bullat.service.expiration;

import io.quarkus.arc.Unremovable;
import jakarta.enterprise.context.ApplicationScoped;

import java.time.LocalDate;

/**
 * Service able to compute the expiration date for a gift deposit
 */
@ApplicationScoped
@Unremovable
public class GiftExpirationService implements ExpirationService {
    @Override
    public LocalDate generateExpirationDate(LocalDate from) {
        return from.plusDays(365 - 1);
    }
}
