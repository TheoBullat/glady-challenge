package org.perso.theo.bullat;

import jakarta.ws.rs.core.Application;
import org.eclipse.microprofile.openapi.annotations.OpenAPIDefinition;
import org.eclipse.microprofile.openapi.annotations.enums.SecuritySchemeType;
import org.eclipse.microprofile.openapi.annotations.info.Contact;
import org.eclipse.microprofile.openapi.annotations.info.Info;
import org.eclipse.microprofile.openapi.annotations.security.SecurityScheme;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

@OpenAPIDefinition(
        info = @Info(
                title = "Challenge Glady API",
                version = "1.0.0",
                contact = @Contact(
                        name = "Theo Bullat",
                        email = "bullat.theo@hotmail.fr")
        ),
        tags = {
                @Tag(name = "Challenge", description = "Application Challenge dans le cadre du test Glady")
        }
)
@SecurityScheme(securitySchemeName = "API token", type = SecuritySchemeType.HTTP, scheme = "bearer", bearerFormat = "Bearer <token>", description = "Bearer token used to authenticate user to the API with the http header `Authorization`")
public class ChallengeApplication extends Application {
}