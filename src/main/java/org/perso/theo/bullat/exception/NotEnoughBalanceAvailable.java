package org.perso.theo.bullat.exception;

/**
 * Exception that represent the fact that a company doesn't have the balance to make a deposit to a user
 */
public class NotEnoughBalanceAvailable extends RuntimeException {

    public NotEnoughBalanceAvailable(String message) {
        super(message);
    }
}
