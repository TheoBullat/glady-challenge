package org.perso.theo.bullat.utils;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.quarkus.jackson.ObjectMapperCustomizer;
import jakarta.inject.Singleton;

import java.text.SimpleDateFormat;
import java.util.TimeZone;

/**
 * ObjectMapperCustomizer that allow us to set different properties that we want about serialisation and deserialisation
 * over the whole application
 */
@Singleton
public class JacksonModuleCustomizer implements ObjectMapperCustomizer {
    public static final String DATE_FORMAT_STR = "yyyy-MM-dd";

    @Override
    public void customize(ObjectMapper objectMapper) {
        objectMapper.setDateFormat(new SimpleDateFormat(DATE_FORMAT_STR));
        objectMapper.setTimeZone(TimeZone.getTimeZone("UTC"));
        objectMapper.setDefaultPropertyInclusion(JsonInclude.Value.construct(JsonInclude.Include.NON_NULL, JsonInclude.Include.ALWAYS));
        objectMapper.enable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
    }
}
