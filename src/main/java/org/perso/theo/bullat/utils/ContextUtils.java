package org.perso.theo.bullat.utils;


import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;

import java.util.UUID;

/**
 * Class used to manage Thread local correlationId
 */
@Slf4j
public final class ContextUtils {
    public static final String CID_MDC_KEY = "request.id";

    /**
     * Private constructor
     */
    private ContextUtils() {
    }

    public static void setCorrelationId(String correlationId) {
        if (correlationId == null) {
            correlationId = UUID.randomUUID().toString();
        }

        MDC.put(CID_MDC_KEY, correlationId);
    }

    public static void removeCorrelationId() {
        MDC.remove(CID_MDC_KEY);
    }
}
