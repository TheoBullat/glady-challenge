package org.perso.theo.bullat.dto;

import lombok.Data;
import lombok.experimental.Accessors;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import java.util.List;

/**
 * Body response given after a deposit have been made to a user
 * For each expirationDate a sum of deposit will be represented in amount and listed in details
 */
@Data
@Accessors(chain = true)
public class BalanceDTO {
    @Schema(description = "Name of the user for which we have a deposit", example = "user")
    private String username;
    @Schema(description = "List of deposit grouped by expiration date")
    private List<BalanceDetailsDTO> details;
}
