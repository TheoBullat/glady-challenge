package org.perso.theo.bullat.dto;

import io.smallrye.common.constraint.NotNull;
import lombok.Data;
import lombok.experimental.Accessors;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

/**
 * Body that is given to authenticate
 */

@Data
@Accessors(chain = true)
public class AuthenticationDTO {
    @NotNull
    @Schema(description = "Name of the user that try to authenticate", example = "user")
    private String username;

    @NotNull
    @Schema(description = "Password should be 'secured'... Literally", example = "secured")
    private String password;

    @Schema(description = "If given, the company will be attached to the user and he will be able to manage deposits for this company", example = "Tesla")
    private String company;
}
