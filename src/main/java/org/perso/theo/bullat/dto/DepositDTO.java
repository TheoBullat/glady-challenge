package org.perso.theo.bullat.dto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import lombok.experimental.Accessors;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

/**
 * Body given to make a deposit for a user
 */
@Data
@Accessors(chain = true)
public class DepositDTO {
    @NotBlank
    @Schema(description = "Name of the user for which we have a deposit", example = "user")
    private String username;
    @NotNull
    @Schema(description = "Amount of the deposit that the user will be able to use", example = "100")
    private Long amount;
}
