package org.perso.theo.bullat.dto;

import lombok.Data;
import lombok.experimental.Accessors;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import java.time.LocalDate;

/**
 * Body response given after a deposit have been made to a user
 * For each expirationDate a sum of deposit will be represented in amount
 */
@Data
@Accessors(chain = true)
public class BalanceDetailsDTO {
    @Schema(description = "Date of expiration of a deposit", example = "2025-01-06")
    private LocalDate expirationDate;
    @Schema(description = "Amount of the deposit that the user will be able to use", example = "100")
    private Long amount;
}
