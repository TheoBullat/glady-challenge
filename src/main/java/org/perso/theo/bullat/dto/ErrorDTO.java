package org.perso.theo.bullat.dto;

import lombok.Data;
import lombok.experimental.Accessors;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

/**
 * Body Response after an Exception occured
 */
@Data
@Accessors(chain = true)
public class ErrorDTO {
    @Schema(description = "Http method of the request that failed", example = "POST")
    private String httpMethod;
    @Schema(description = "Resource that has failed", example = "/gift")
    private String resource;
    @Schema(description = "Http status of the response", example = "Bad Request")
    private String status;
    @Schema(description = "Explication of the failure", example = "Tesla has not enough balance to do the transaction")
    private String message;
}
