package org.perso.theo.bullat.controller.filter;


import jakarta.ws.rs.container.ContainerRequestContext;
import lombok.extern.slf4j.Slf4j;
import org.jboss.resteasy.reactive.server.ServerRequestFilter;
import org.perso.theo.bullat.utils.ContextUtils;

import java.io.IOException;


/**
 * Http filter used to extract correlation id
 */
@Slf4j
public class CorrelationIdFilter {

    /**
     * Request ID header
     */
    public static final String REQUEST_ID_HEADER = "X-Request-Id";

    @ServerRequestFilter(preMatching = true, priority = 1)
    public void filter(ContainerRequestContext containerRequestContext) {
        String correlationId = containerRequestContext.getHeaderString(REQUEST_ID_HEADER);
        ContextUtils.setCorrelationId(correlationId);
    }
}
