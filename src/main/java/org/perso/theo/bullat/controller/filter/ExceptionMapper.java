package org.perso.theo.bullat.controller.filter;

import io.netty.handler.codec.http.HttpResponseStatus;
import io.quarkus.security.ForbiddenException;
import io.quarkus.security.UnauthorizedException;
import io.vertx.core.http.HttpServerRequest;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.UriInfo;
import lombok.extern.slf4j.Slf4j;
import org.jboss.resteasy.reactive.server.ServerExceptionMapper;
import org.perso.theo.bullat.dto.ErrorDTO;
import org.perso.theo.bullat.exception.NotEnoughBalanceAvailable;

/**
 * Mapper used to define the response given after some Exception occured
 */
@Slf4j
public class ExceptionMapper {
    public static final String HTTP_METHOD_KEY = "http_method";

    @Context
    HttpServerRequest request;

    @Context
    UriInfo uriInfo;


    @ServerExceptionMapper
    public Response onForbiddenException(final ForbiddenException exception) {
        log.error(exception.getMessage());

        return createResponse(exception, HttpResponseStatus.FORBIDDEN);
    }

    @ServerExceptionMapper
    public Response onUnauthorizedException(final UnauthorizedException exception) {
        log.error(exception.getMessage());

        return createResponse(exception, HttpResponseStatus.UNAUTHORIZED);
    }

    @ServerExceptionMapper
    public Response onNotEnoughBalanceAvailable(final NotEnoughBalanceAvailable exception) {
        log.warn(exception.getMessage());

        return createResponse(exception, HttpResponseStatus.BAD_REQUEST);
    }

    /**
     * Create an error response with the exception message and a given status
     *
     * @param exception      Exception that occured during a treatment
     * @param responseStatus Status that we want to give on the response
     * @return Generated Response
     */
    private Response createResponse(Exception exception, HttpResponseStatus responseStatus) {
        final ErrorDTO error = new ErrorDTO()
                .setHttpMethod(request.method().name())
                .setResource(uriInfo.getPath())
                .setStatus(responseStatus.reasonPhrase())
                .setMessage(exception.getMessage());

        return Response.status(responseStatus.code())
                .type(MediaType.APPLICATION_JSON)
                .entity(error)
                .build();
    }
}
