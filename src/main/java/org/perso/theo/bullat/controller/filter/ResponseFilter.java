package org.perso.theo.bullat.controller.filter;

import io.vertx.core.http.impl.HttpUtils;
import jakarta.ws.rs.container.ContainerRequestContext;
import jakarta.ws.rs.container.ContainerResponseContext;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.UriInfo;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.jboss.resteasy.reactive.server.ServerRequestFilter;
import org.jboss.resteasy.reactive.server.ServerResponseFilter;
import org.perso.theo.bullat.utils.ContextUtils;

/**
 * Http filter used at the beginning and the end of a request
 */
@Slf4j
public class ResponseFilter {
    @Context
    UriInfo info;

    @ServerRequestFilter
    public void filter(ContainerRequestContext requestContext) {
        log.info("Request start: {}", HttpUtils.normalizePath(info.getPath()));
        long requestStartTime = System.currentTimeMillis();
        requestContext.setProperty("requestStartTime", requestStartTime);
    }

    @ServerResponseFilter
    public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext) {
        if (requestContext != null) {
            if (CollectionUtils.isNotEmpty(requestContext.getPropertyNames()) && requestContext.getPropertyNames().contains("requestStartTime")) {
                long responseTime = System.currentTimeMillis() - (long) requestContext.getProperty("requestStartTime");
                log.info("Response sent, uri_query={}, status={}, ResponseTime={}", requestContext.getMethod() + " " + requestContext.getUriInfo().getPath(), responseContext.getStatus(), responseTime);
            } else {
                log.info("Response sent, uri_query={}, status={}", requestContext.getMethod() + " " + requestContext.getUriInfo().getPath(), responseContext.getStatus());
            }
        }
        // Clear context
        ContextUtils.removeCorrelationId();
    }
}

