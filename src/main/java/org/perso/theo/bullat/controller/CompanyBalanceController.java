package org.perso.theo.bullat.controller;

import jakarta.annotation.security.RolesAllowed;
import jakarta.enterprise.context.RequestScoped;
import jakarta.inject.Inject;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.microprofile.jwt.JsonWebToken;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponses;
import org.perso.theo.bullat.dto.ErrorDTO;
import org.perso.theo.bullat.service.CompanyBalanceService;

import static org.perso.theo.bullat.service.TokenGeneratorService.COMPANY_MANAGER;
import static org.perso.theo.bullat.service.TokenGeneratorService.ROLE_COMPANY;

/**
 * Controller of the endpoint that manage the balance of a company
 */
@Slf4j
@RequestScoped
@Path("/company")
public class CompanyBalanceController {

    @Inject
    CompanyBalanceService companyBalanceService;

    @Inject
    JsonWebToken jwt;

    @Operation(
            summary = "Credit company",
            description = "Add some credit to a company in order to be able to make future deposit for a user from this company.<br/>" +
                    "The company is automatically selected from the company attached on the bearer"
    )
    @APIResponses(value = {
            @APIResponse(responseCode = "401", description = "Unauthorized", content = @Content(mediaType = MediaType.APPLICATION_JSON, schema = @Schema(implementation = ErrorDTO.class))),
            @APIResponse(responseCode = "403", description = "Forbidden", content = @Content(mediaType = MediaType.APPLICATION_JSON, schema = @Schema(implementation = ErrorDTO.class))),
            @APIResponse(responseCode = "200", description = "Successful request", content = @Content(mediaType = MediaType.TEXT_PLAIN, schema = @Schema(implementation = String.class)))
    })
    @POST
    @Produces(MediaType.TEXT_PLAIN)
    @RolesAllowed(ROLE_COMPANY)
    @Path("credit/{amount}")
    public String creditCompany(@PathParam("amount") Long amount) {
        String companyManaged = jwt.getClaim(COMPANY_MANAGER);
        Long newAmount = companyBalanceService.creditCompany(companyManaged, amount);
        return String.format("New balance for %s is %d", companyManaged, newAmount);
    }
}
