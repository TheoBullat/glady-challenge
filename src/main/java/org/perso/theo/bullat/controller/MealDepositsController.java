package org.perso.theo.bullat.controller;

import io.quarkus.security.identity.SecurityIdentity;
import jakarta.annotation.security.RolesAllowed;
import jakarta.inject.Inject;
import jakarta.validation.Valid;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.microprofile.jwt.JsonWebToken;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponses;
import org.perso.theo.bullat.dao.domain.TypeEnum;
import org.perso.theo.bullat.dto.BalanceDTO;
import org.perso.theo.bullat.dto.DepositDTO;
import org.perso.theo.bullat.dto.ErrorDTO;
import org.perso.theo.bullat.service.UserDepositsService;

import static org.perso.theo.bullat.service.TokenGeneratorService.*;

/**
 * This controller give access to endpoint to manage balance around meals
 */

@Slf4j
@Path("/meal")
public class MealDepositsController {

    @Inject
    SecurityIdentity securityIdentity;

    @Inject
    UserDepositsService depositsService;

    @Inject
    JsonWebToken jwt;

    @Operation(
            summary = "Deposit gift to user",
            description = "Make a deposit to a user from your company. The company must have the capacity asked. If your company is missing some credit you can add some with /company/credit/{amount}"
    )
    @APIResponses(value = {
            @APIResponse(responseCode = "401", description = "Unauthorized", content = @Content(mediaType = MediaType.APPLICATION_JSON, schema = @Schema(implementation = ErrorDTO.class))),
            @APIResponse(responseCode = "403", description = "Forbidden", content = @Content(mediaType = MediaType.APPLICATION_JSON, schema = @Schema(implementation = ErrorDTO.class))),
            @APIResponse(responseCode = "200", description = "Successful request", content = @Content(mediaType = MediaType.APPLICATION_JSON, schema = @Schema(implementation = BalanceDTO.class)))
    })
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed(ROLE_COMPANY)
    public BalanceDTO depositMealForUser(@Valid DepositDTO deposit) {
        String actualUser = securityIdentity.getPrincipal().getName();
        String companyManaged = jwt.getClaim(COMPANY_MANAGER);
        return depositsService.creditUser(deposit, TypeEnum.MEAL, actualUser, companyManaged);
    }

    @Operation(
            summary = "Retrieve user's balance",
            description = "Retrieve the user's actual balance for meal. You will see the future date of expiration of each deposits you currently have. Expired deposit won't be shown"
    )
    @APIResponses(value = {
            @APIResponse(responseCode = "401", description = "Unauthorized", content = @Content(mediaType = MediaType.APPLICATION_JSON, schema = @Schema(implementation = ErrorDTO.class))),
            @APIResponse(responseCode = "403", description = "Forbidden", content = @Content(mediaType = MediaType.APPLICATION_JSON, schema = @Schema(implementation = ErrorDTO.class))),
            @APIResponse(responseCode = "200", description = "Successful request", content = @Content(mediaType = MediaType.APPLICATION_JSON, schema = @Schema(implementation = BalanceDTO.class)))
    })
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed(ROLE_USER)
    public BalanceDTO getUserBalance() {
        String actualUser = securityIdentity.getPrincipal().getName();
        return depositsService.getBalance(actualUser, TypeEnum.MEAL);
    }
}
