package org.perso.theo.bullat.controller;

import jakarta.inject.Inject;
import jakarta.validation.Valid;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponses;
import org.perso.theo.bullat.dto.AuthenticationDTO;
import org.perso.theo.bullat.dto.ErrorDTO;
import org.perso.theo.bullat.service.TokenGeneratorService;

/**
 * Controller to retrieve a bearer that will be used on every other endpoints
 */

@Slf4j
@Path("/login")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.TEXT_PLAIN)
public class LoginController {

    @Inject
    TokenGeneratorService tokenGeneratorService;

    @Operation(
            summary = "Post login",
            description = "Retrieve a bearer token"
    )
    @APIResponses(value = {
            @APIResponse(responseCode = "403", description = "Forbidden", content = @Content(mediaType = MediaType.APPLICATION_JSON, schema = @Schema(implementation = ErrorDTO.class))),
            @APIResponse(responseCode = "200", description = "Successful request", content = @Content(mediaType = MediaType.TEXT_PLAIN, schema = @Schema(implementation = String.class)))
    })
    @POST
    public String getToken(@Valid AuthenticationDTO authent) {
        tokenGeneratorService.checkUserCredentials(authent.getPassword());
        return tokenGeneratorService.generateTokenWithRole(authent);
    }
}
