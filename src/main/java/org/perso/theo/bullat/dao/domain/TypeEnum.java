package org.perso.theo.bullat.dao.domain;

import lombok.Getter;
import org.perso.theo.bullat.service.expiration.ExpirationService;
import org.perso.theo.bullat.service.expiration.GiftExpirationService;
import org.perso.theo.bullat.service.expiration.MealExpirationService;

/**
 * Represent the different type of deposit we can do
 * Each type has its own way to compute the expiration date
 */

@Getter
public enum TypeEnum {
    GIFT(GiftExpirationService.class),
    MEAL(MealExpirationService.class);

    private Class<? extends ExpirationService> expirationService;

    TypeEnum(Class<? extends ExpirationService> expirationDateService) {
        this.expirationService = expirationDateService;
    }
}
