package org.perso.theo.bullat.dao;

import io.quarkus.hibernate.orm.panache.PanacheEntity;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import jakarta.persistence.UniqueConstraint;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.Optional;

/**
 * Entity that represent the balance of a companies
 * For each companies, a unique raw should represent its balance
 */
@Entity
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper=true)
@Table(name = "company_balance", uniqueConstraints = @UniqueConstraint(name = "uc_company", columnNames = "company"))
public class CompanyBalanceDAO extends PanacheEntity {

    @NotEmpty
    private String company;

    @NotNull
    private Long amount;

    public static Optional<CompanyBalanceDAO> findByCompany(String company) {
        return find("company", company).firstResultOptional();
    }
}
