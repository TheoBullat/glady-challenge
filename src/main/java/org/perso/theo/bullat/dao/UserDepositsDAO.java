package org.perso.theo.bullat.dao;

import io.quarkus.hibernate.orm.panache.PanacheEntity;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.perso.theo.bullat.dao.domain.TypeEnum;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Represent a deposit that has been done to a user
 */

@Entity
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper=true)
@Table(name = "user_deposits")
public class UserDepositsDAO extends PanacheEntity {

    @NotEmpty
    private String username;

    @NotNull
    private Long amount;

    @NotNull
    @Column(name = "expire_at")
    private LocalDate expireAt;

    @NotNull
    private TypeEnum type;

    @NotNull
    @Column(name = "credited_by")
    private String creditedBy;

    @NotNull
    @Column(name = "credited_at")
    private LocalDateTime creditedAt;

    public static List<UserDepositsDAO> findNotExpiredDepositsByType(String username, TypeEnum type) {
        return find("username = ?1 and type = ?2 and expireAt >= ?3", username, type, LocalDate.now()).list();
    }
}
